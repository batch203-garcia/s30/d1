//Aggregation 


db.fruits.aggregate([{
    $match: {
        onSale: true
    }
}]);


db.fruits.aggregate([{
    $match: {
        onSale: true
    }
}, {
    $group: {
        _id: "$supplier_id",
        total: {
            $sum: "$stock"
        }
    }
}]);

db.fruits.aggregate([{
    $match: {
        onSale: true
    }
}, {
    $group: {
        _id: "$supplier_id",
        total: {
            $sum: "$stock"
        }
    }
}, {
    $project: {
        id: {
            _id: 0
        }
    }
}]);

//sort
db.fruits.aggregate([{
    $match: {
        onSale: true
    }
}, {
    $group: {
        _id: "$supplier_id",
        total: {
            $sum: "$stock"
        }
    }
}, {
    $sort: {
        total: 1
    }
}]);

//Aggregating based on array fields


db.fruits.aggregate([{
    $unwind: "$origin"
}, {
    $group: {
        _id: "$origin",
        kinds: {
            $sum: 1
        }
    }
}]);

//count all yellow fruits

db.fruits.aggregate([{
    $match: {
        color: "Yellow"
    }
}, {
    $count: "Yellow Fruits"
}]);

//average
db.fruits.aggregate([{
    $match: {
        color: "Yellow"
    }
}, {
    $group: {
        _id: "$color",
        yellow_fruits_stock: {
            $avg: "$stock"
        }
    }
}]);


//min and max
db.fruits.aggregate([{
    $match: {
        color: "Yellow"
    }
}, {
    $group: {
        _id: "$color",
        yellow_fruits_stock: {
            $min: "$stock"
        }
    }
}]);